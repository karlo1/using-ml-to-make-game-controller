import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms
from torchvision.models import mobilenet_v3_large
from PIL import Image
import pandas as pd
from sklearn.model_selection import train_test_split 
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import seaborn as sns
from torch.optim import lr_scheduler 
from torchvision import models
import cv2
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_auc_score, confusion_matrix
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import label_binarize
import numpy as np

# Define your dataset class
class CustomDataset(Dataset):
    def __init__(self, csv_path, transform=None):
        self.data = pd.read_csv(csv_path)
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        img_path = self.data.iloc[idx, 0]
        label = self.data.iloc[idx, 1]

        # Load image in grayscale
        img = Image.open(img_path)#.convert("L")

        if self.transform:
            img = self.transform(img)

        return img, label

# Define ResNet model
class BaggedMobileNetV3(nn.Module):
    def __init__(self, num_models):
        super(BaggedMobileNetV3, self).__init__()
        self.num_models = num_models
        self.models = [self.create_model() for _ in range(num_models)]

    def create_model(self):
        model = models.mobilenet_v3_large(pretrained=True)
        model.conv1 = nn.Conv2d(3, 64, kernel_size=(5, 5), stride=(2, 2), padding=(3, 3), bias=False)
        model.classifier = nn.Sequential(
            nn.Linear(model.classifier[0].in_features, 512),
            nn.ReLU(inplace=True),
            nn.Linear(512, 10)  # Assuming you have 10 classes
        )
        return model

    def forward(self, x):
        outputs = [model(x) for model in self.models]
        return torch.stack(outputs).mean(dim=0)

# Define ResNet model
class BaggedResnet(nn.Module):
    def __init__(self, num_models):
        super(BaggedResnet, self).__init__()
        self.num_models = num_models
        self.models = [self.create_model() for _ in range(num_models)]

    def create_model(self):
        model = models.resnet34(pretrained=True)
        model.conv1 = nn.Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        # Add Batch Normalization after the convolutional layers
        model.bn1 = nn.BatchNorm2d(64)

        # Change the classifier for binary classification
        model.fc = nn.Linear(model.fc.in_features, 10)

        # Add Batch Normalization before the fully connected layer
        model.bn2 = nn.BatchNorm1d(10)
        return model

    def forward(self, x):
        outputs = [model(x) for model in self.models]
        return torch.stack(outputs).mean(dim=0)

# Adjusted data augmentation with random lighting changes and dynamic normalization
class LightingNoise(object):
    def __init__(self, noise_factor=0.05):
        self.noise_factor = noise_factor

    def __call__(self, img):
        # Apply random noise
        noise = torch.randn_like(img) * self.noise_factor

        # Add noise to the image
        img = img + noise

        # Clip values to be in the valid range [0, 1]
        img = torch.clamp(img, 0, 1)

        return img

class ToTensor(object):
    def __call__(self, img):
        # Convert the PIL image to a grayscale tensor
        to_tensor = transforms.ToTensor()
        img = to_tensor(img)

        # If the image is grayscale, repeat the single channel to create three channels
        #if img.shape[0] == 1:
        #    img = img.repeat(3, 1, 1)

        # Normalize the image tensor
        normalize = transforms.Normalize(mean=[0.5925, 0.5734, 0.5789], std=[0.1742, 0.1713, 0.1785])
        img = normalize(img)

        return img

def load_split_train_test(csv_file, test_size=0.2):
    # Load the CSV file
    data = pd.read_csv(csv_file)

    # Split data into training and evaluation sets
    train_data, eval_data = train_test_split(data, test_size=test_size, random_state=42)

    return train_data, eval_data

# Define ResNet model
#model = models.mobilenet_v3_large(pretrained=True)                          #resnet50(pretrained=True)  # You can choose different versions like resnet34, resnet50, etc.

#model.conv1 = nn.Conv2d(3, 64, kernel_size=(5, 5), stride=(2, 2), padding=(3, 3), bias=False)
# Change the classifier for binary classification
#model.fc = nn.Linear(model.fc.in_features, 10)
# Modify the classifier for your task
#model.classifier = nn.Sequential(
#    nn.Linear(model.classifier[0].in_features, 512),
#    nn.ReLU(inplace=True),
#    nn.Linear(512, 10)  # Assuming you have 10 classes
#)
# Create bagged model """'cuda' if torch.cuda.is_available() else""" 
# Create class mappings for each group




# Define transformations with normalization and standardization
transform = transforms.Compose([
   ToTensor(),
   transforms.ColorJitter(brightness=0.4, contrast=0.1, saturation=0.5, hue=0.1),
   #LightingNoise(),
])

# Define transformations with normalization and standardization
transformv2 = transforms.Compose([
    ToTensor(),
   
])

class_mapping_group0 = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0}
class_mapping_group1 = {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 1, 9: 1}

device = torch.device('cuda' if torch.cuda.is_available() else'cpu')
bagged_model_group0 = BaggedResnet(num_models=2)
bagged_model_group1 = BaggedResnet(num_models=2)

# Assuming you have a CSV file named "data.csv" with columns "image_path" and "class"
csv_file = r"C:\Users\Mikkel\source\repos\PythonApp_0001\processed dataset RGB v2\annotations.csv"

# You can change the test_size argument to adjust the split ratio
train_data, eval_data = load_split_train_test(csv_file, test_size=0.2)

# Apply class mappings to the datasets
train_data_group0 = train_data[train_data["Class Label"].map(class_mapping_group0).notna()]
eval_data_group0 = eval_data[eval_data['class'].map(class_mapping_group0).notna()]

train_data_group1 = train_data[train_data['class'].map(class_mapping_group1).notna()]
eval_data_group1 = eval_data[eval_data['class'].map(class_mapping_group1).notna()]

# Create custom datasets for training and evaluation
train_dataset_group0 = CustomDataset(data=train_data_group0, transform=transform)
eval_dataset_group0 = CustomDataset(data=eval_data_group0, transform=transformv2)

# Create custom datasets for training and evaluation
train_dataset_group1 = CustomDataset(data=train_data_group1 , transform=transform)
eval_dataset_group1 = CustomDataset(data=eval_data_group1, transform=transformv2)


# Set batch size
batch_size = 40

# Create data loaders for training and evaluation
train_data_loader_group0 = DataLoader(dataset=train_dataset_group0, batch_size=batch_size, shuffle=True)
eval_data_loader_group0 = DataLoader(dataset=eval_dataset_group0, batch_size=batch_size, shuffle=False)

# Create data loaders for training and evaluation
train_data_loader_group1 = DataLoader(dataset=train_dataset_group1, batch_size=batch_size, shuffle=True)
eval_data_loader_group1 = DataLoader(dataset=eval_dataset_group1, batch_size=batch_size, shuffle=False)

# Define loss function and optimizer
criterion = nn.CrossEntropyLoss()

params = []
for model in bagged_model.models:
    params += list(model.parameters())

optimizer_group0 = optim.SGD(params, lr=0.0001, momentum=0.8)
optimizer_group1 = optim.SGD(params, lr=0.0001, momentum=0.8)
#optimizer = optim.Adam(params, lr=0.001, betas=(0.9,0.999))

scheduler = lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.1)

# Define label mapping
label_mapping = {'block': 0, 'crouch': 1, 'dodge': 2, 'foward': 3, 'jab': 4, 'kick': 5, 'slash': 6, 'speciel': 7, 'turn-left': 8, 'turn-right': 9}
num_classes = len(label_mapping)
train_losses = []

# Training loop
num_epochs = 50

print(device)
# Ensure all models and the bagged model are on the same device
for model in bagged_model.models:
    model.to(device)

# Evaluation function
def evaluate_model_with_metrics(model, dataloader):
    model.eval()
    correct = 0
    total = 0

    predictions = []
    ground_truth = []

    with torch.no_grad():
        for inputs, labels in dataloader:
            # Sending frame to device
            inputs = inputs.to(device)

            # Convert string labels to integers using label mapping
            labels = torch.tensor([label_mapping[label] for label in labels]).to(device)

            outputs = model(inputs)

            _, predicted = torch.max(outputs.data, 1)

            total += labels.size(0)
            correct += (predicted == labels).sum().item()

            predictions.extend(predicted.cpu().numpy())
            ground_truth.extend(labels.cpu().numpy())

    accuracy = correct / total
    precision = precision_score(ground_truth, predictions, average='weighted', zero_division=1)
    recall = recall_score(ground_truth, predictions, average='weighted')
    f1 = f1_score(ground_truth, predictions, average='weighted')

    # Convert integer labels to one-hot encoded format
    ground_truth_onehot = label_binarize(ground_truth, classes=np.arange(num_classes))
    predictions_onehot = label_binarize(predictions, classes=np.arange(num_classes))

    # Use roc_auc_score with multi_class='ovr'
    roc_auc = roc_auc_score(ground_truth_onehot, predictions_onehot, multi_class='ovr')

    return accuracy, precision, recall, f1, roc_auc, predictions, ground_truth

best_accuracy = 0.0
best_epoch = 0

for epoch in range(num_epochs):
    bagged_model_group0.train()
    running_loss = 0.0
    for inputs, labels in train_data_loader_group1:
        
        # Sending frame to device
        inputs = inputs.to(device)
        # Convert string labels to integers using label mapping
        labels = torch.tensor([label_mapping[label] for label in labels]).to(device)

        # Zero the gradients
        optimizer.zero_grad()

        # Forward pass
        outputs = model(inputs)
        loss = criterion(outputs, labels)

        # Backward pass and optimization
        loss.backward()
        optimizer.step()

        # Accumulate running loss
        running_loss += loss.item()

    # Calculate average training loss for the epoch
    avg_loss = running_loss / len(train_data_loader_group1)
    train_losses.append(avg_loss)
    print(f'Epoch {epoch+1}/{num_epochs}, Loss: {avg_loss}')

    # Run evaluation
    accuracy, precision, recall, f1, roc_auc, predictions, ground_truth = evaluate_model_with_metrics(bagged_model_group0, eval_data_loader_group0)
    print(f'Accuracy on validation set: {accuracy}')
    print(f'Precision on validation set: {precision}')
    print(f'Recall on validation set: {recall}')
    print(f'F1 score on validation set: {f1}')
    print(f'ROC AUC on validation set: {roc_auc}')

    # Save the model if the current accuracy is better than the best accuracy
    if accuracy > best_accuracy:
        best_accuracy = accuracy
        best_epoch = epoch
        torch.save({
            'epoch': epoch + 1,
            'model_state_dict': bagged_model_group1.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': loss,
            }, f'best_model_epoch_{epoch + 1}.pth')
    #scheduler.step()

# Save the trained model
torch.save({
    'epoch': num_epochs,
    'model_state_dict': bagged_model_group0.state_dict(),
    'optimizer_state_dict': optimizer.state_dict(),
    'loss': loss,
    }, 'trained_model.pth')

for epoch in range(num_epochs):
    bagged_model_group1.train()
    running_loss = 0.0
    for inputs, labels in train_data_loader_group0:
        
        # Sending frame to device
        inputs = inputs.to(device)
        # Convert string labels to integers using label mapping
        labels = torch.tensor([label_mapping[label] for label in labels]).to(device)

        # Zero the gradients
        optimizer.zero_grad()

        # Forward pass
        outputs = model(inputs)
        loss = criterion(outputs, labels)

        # Backward pass and optimization
        loss.backward()
        optimizer.step()

        # Accumulate running loss
        running_loss += loss.item()

    # Calculate average training loss for the epoch
    avg_loss = running_loss / len(train_data_loader_group1)
    train_losses.append(avg_loss)
    print(f'Epoch {epoch+1}/{num_epochs}, Loss: {avg_loss}')

    # Run evaluation
    accuracy, precision, recall, f1, roc_auc, predictions, ground_truth = evaluate_model_with_metrics(bagged_model_group1, eval_data_loader_group1)
    print(f'Accuracy on validation set: {accuracy}')
    print(f'Precision on validation set: {precision}')
    print(f'Recall on validation set: {recall}')
    print(f'F1 score on validation set: {f1}')
    print(f'ROC AUC on validation set: {roc_auc}')

    # Save the model if the current accuracy is better than the best accuracy
    if accuracy > best_accuracy:
        best_accuracy = accuracy
        best_epoch = epoch
        torch.save({
            'epoch': epoch + 1,
            'model_state_dict': bagged_model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': loss,
            }, f'best_model_epoch_{epoch + 1}.pth')
    #scheduler.step()

# Save the trained model
torch.save({
    'epoch': num_epochs,
    'model_state_dict': bagged_model_group1.state_dict(),
    'optimizer_state_dict': optimizer.state_dict(),
    'loss': loss,
    }, 'trained_model.pth')



# Run evaluation with metrics
#accuracy, precision, recall, f1, roc_auc, predictions, ground_truth = evaluate_model_with_metrics(bagged_model, eval_data_loader)
#print(f'Accuracy on validation set: {accuracy}')
#print(f'Precision on validation set: {precision}')
#print(f'Recall on validation set: {recall}')
##print(f'F1 score on validation set: {f1}')
#print(f'ROC AUC on validation set: {roc_auc}')

# Plot confusion matrix
def plot_confusion_matrix(y_true, y_pred, classes):
    cm = confusion_matrix(y_true, y_pred)
    plt.figure(figsize=(10, 8))
    sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=classes, yticklabels=classes)
    plt.xlabel('Predicted')
    plt.ylabel('True')
    plt.title('Confusion Matrix')
    plt.show()

# Example usage:
# Assuming 'ground_truth' and 'predictions' are lists or arrays containing true labels and predicted labels
plot_confusion_matrix(ground_truth, predictions, classes=['kick', 'crouch', 'slash', 'speciel', 'dogde', 'foward', 'turn-right', 'jab', 'turn-left', 'block'])


# Plot loss over epochs
def plot_loss_over_epochs(train_losses):
    """
    Plot the loss value over epochs.

    Parameters:
    - train_losses (list): List of training losses for each epoch.
    """
    plt.figure(figsize=(10, 6))
    plt.plot(range(1, len(train_losses) + 1), train_losses, marker='o', linestyle='-', color='b')
    plt.title('Training Loss Over Epochs')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.grid(True)
    plt.show()

# Call the function to plot the loss
plot_loss_over_epochs(train_losses)





