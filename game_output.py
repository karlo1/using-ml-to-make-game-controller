import keyboard
import time
import mouse
import torch.nn as nn
import cv2
import torch
import torchvision.transforms as transforms
from torchvision.models import mobilenet_v3_large
import numpy as np
import torch.nn.functional as F
from torchvision import models
from torch.autograd import Function

label_mapping = {
    0: 'block',
    1: 'crouch',
    2: 'dogde',
    3: 'foward',
    4: 'jab',
    5: 'kick',
    6: 'slash',
    7: 'speciel',
    8: 'turn-left',
    9: 'turn-right'
}

class GradCAM:
    def __init__(self, model, target_layer):
        self.model = model
        self.target_layer = target_layer
        self.gradients = None

        # Register hooks for the target layer
        self.hook = self.register_hooks()

    def register_hooks(self):
        def hook_fn(module, grad_input, grad_output):
            self.gradients = grad_output[0]

        target_layer = self.model._modules[self.target_layer]
        hook = target_layer.register_forward_hook(hook_fn)
        return hook

    def remove_hooks(self):
        self.hook.remove()

    def generate_cam(self, input_tensor, class_idx):
        # Forward pass
        self.model.zero_grad()
        output = self.model(input_tensor)

        # Backward pass to get gradients
        one_hot_output = torch.zeros_like(output)
        one_hot_output[:, class_idx] = 1.0
        output.backward(gradient=one_hot_output, retain_graph=True)

        # Global average pooling of gradients
        weights = torch.mean(self.gradients, dim=(2), keepdim=True)

        # Assuming 'features.11.block.2.fc2.weight' is the last layer's weight tensor
        last_layer_weight = self.model.features[11].block[2].fc2.weight
        # Weighted sum to get the CAM
        cam = torch.sum(weights * last_layer_weight, dim=0)
        cam = F.relu(cam)

        return cam.detach().numpy()
#from Main import ModifiedMobileNetV3
class ModifiedMobileNetV3(nn.Module):
    def __init__(self, num_classes=1000, input_channels=1):
        super(ModifiedMobileNetV3, self).__init__()
        
        # Load the pre-trained MobileNetV3 model with small architecture
        original_model = mobilenet_v3_large(pretrained=True)  # Change here
        
        # Modify the input layer to accept grayscale images
        self.features = original_model.features
        self.features[0][0] = nn.Conv2d(input_channels, 16, kernel_size=3, stride=2, padding=1, bias=False)
        
        # Classifier
        self.classifier = original_model.classifier

        # Change the classifier for binary classification
        self.classifier[-1] = nn.Linear(self.classifier[-1].in_features, num_classes)
        
    def forward(self, x):
        x = self.features(x)
        x = x.mean([2, 3])  # Global average pooling
        x = self.classifier(x)
        return x


# Load your pre-trained PyTorch model
# Create an instance of the model
#model = ModifiedMobileNetV3(num_classes=10, input_channels=1)
# Define ResNet model
#model = models.resnet18(pretrained=False)  # You can choose different versions like resnet34, resnet50, etc.
# Modify the input layer to accept grayscale images
#model.conv1 = nn.Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
# Change the classifier for binary classification
#model.fc = nn.Linear(model.fc.in_features, 10)
# Define ResNet model
model = models.mobilenet_v3_large(pretrained=False)                          #resnet50(pretrained=True)  # You can choose different versions like resnet34, resnet50, etc.

model.conv1 = nn.Conv2d(3, 64, kernel_size=(5, 5), stride=(2, 2), padding=(3, 3), bias=False)
# Change the classifier for binary classification
#model.fc = nn.Linear(model.fc.in_features, 10)
# Modify the classifier for your task
model.classifier = nn.Sequential(
    nn.Linear(model.classifier[0].in_features, 512),
    nn.ReLU(inplace=True),
    nn.Linear(512, 10)  # Assuming you have 10 classes
)


# Load the pre-trained weights
model_weights = torch.load(r'C:\Users\Mikkel\source\repos\PythonApp_0001\PythonApp_0001\best_model_epoch_16.pth')  # Adjust the path accordingly

# Create a new state dictionary with only the model parameters
new_state_dict = {}
for key, value in model_weights['model_state_dict'].items():
    new_key = key.replace('module.', '')  # If the model was saved using DataParallel
    new_state_dict[new_key] = value

# Load the new state dictionary into the model
model.load_state_dict(new_state_dict)

print(model.state_dict())

# Set the model to evaluation mode
model.eval()



class DilationErosionTransform:
    def __call__(self, img):
        if isinstance(img, np.ndarray):

            # Crop the sides of the image
            image = img[:, 275:-275, :]
            # Resize the image
            image = cv2.resize(image, (244, 244))
            
            # Convert to grayscale
            #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            #img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            # Apply blur with a specified kernel
            image = cv2.GaussianBlur(image, (3, 3), 0)
            # Dilation and erosion
            kernel = np.ones((2, 2), np.uint8)
            image = cv2.dilate(image, kernel, iterations=1)
            image = cv2.erode(image, kernel, iterations=1)

            # Convert NumPy array to PyTorch tensor
            image = torch.from_numpy(image).permute(2, 0, 1).float()

            return image
        else:
            raise TypeError("Input should be a NumPy array")

# Define image transformations including Gaussian blur, dilation, and erosion
transform = transforms.Compose([
    DilationErosionTransform(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])

# Set the confidence threshold
threshold = 0.7

# Open a connection to the camera (use 0 for the default camera)
cap = cv2.VideoCapture(0)


while True:
    # Read a frame from the camera
    ret, frame = cap.read()

    # Preprocess the frame
    input_tensor = transform(frame)
    input_batch = input_tensor.unsqueeze(0)  # Add a batch dimension

    # Perform the inference
    with torch.no_grad():
        output = model(input_batch)

    # Before sigmoid activation
    raw_scores = output[0]
    print("Raw Scores:", raw_scores)


    # Check if the maximum raw score is above the threshold
    if torch.max(raw_scores) > threshold:
        # Apply softmax activation
        probabilities = torch.nn.functional.softmax(raw_scores, dim=0)

        # Get the predicted class and confidence
        predicted_class = torch.argmax(probabilities).item()
        confidence = probabilities[predicted_class].item()

        # Get the label for the predicted class using the imported mapping
        predicted_label = label_mapping.get(predicted_class, f'Unknown Class {predicted_class}')

       

             # Grad-CAM
        target_layer = 'features'  # Choose the appropriate layer name
        cam_generator = GradCAM(model, target_layer)
        cam = cam_generator.generate_cam(input_batch, predicted_class)
        cam = cv2.resize(cam, (frame.shape[1], frame.shape[0]))

        # Normalize the CAM values to the range [0, 1]
        cam = (cam - np.min(cam)) / (np.max(cam) - np.min(cam))

        # Apply the heatmap to the original frame
        heatmap = cv2.applyColorMap(np.uint8(255 * cam), cv2.COLORMAP_JET)
        cam_on_image = heatmap * 0.5 + frame * 0.5  # Weighted sum

        # Display the CAM overlay on the frame
        cv2.imshow("CAM Overlay", cam_on_image)

    else:
        # Handle the case when raw scores are below the threshold
        predicted_class = 9  # Set it to the last class (you can adjust this)
        predicted_label = label_mapping.get(predicted_class, f'Unknown Class {predicted_class}')
        confidence = 1.0  # Set confidence to 100%
        label = f"Class: {predicted_label}, Confidence: {confidence:.2f}"

     # Draw the result on the frame
    label = f"Class: {predicted_label}, Confidence: {confidence:.2f}"
    cv2.putText(frame, label, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2, cv2.LINE_AA)

    # Display the frame
    cv2.imshow("Live Prediction", frame)
    # Remove hooks after visualization
    cam_generator.remove_hooks()

    # Print the prediction if confidence exceeds the threshold
    print(f"Prediction: Class {predicted_class}, Confidence: {confidence:.2f}")


   #case




    time.sleep(0.5)
    # Break the loop if 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
"""
while True:
    # Read a frame from the camera
    ret, frame = cap.read()

    # Preprocess the frame
    input_tensor = transform(frame)
    input_batch = input_tensor

    # Perform the inference
    with torch.no_grad():
        output = model(input_batch)
    
        # Before softmax
    raw_scores = output[0]
    print("Raw Scores:", raw_scores)

    # Convert the output to probabilities using softmax
    probabilities = torch.nn.functional.softmax(output[0], dim=0)

    # Get the predicted class and confidence
    predicted_class = torch.argmax(probabilities).item()
    confidence = probabilities[predicted_class].item()

    
    # Get the label for the predicted class using the imported mapping
    predicted_label = label_mapping.get(predicted_class, f'Unknown Class {predicted_class}')

     # Draw the result on the frame
    label = f"Class: {predicted_label}, Confidence: {confidence:.2f}"
    cv2.putText(frame, label, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)

    # Display the frame
    cv2.imshow("Live Prediction", frame)

    # Check if confidence exceeds the threshold
    if confidence > confidence_threshold:
        print(f"Prediction: Class {predicted_class}, Confidence: {confidence:.2f}")
    
    time.sleep(1)
    # Break the loop if 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
"""
# Release the camera and close the OpenCV window
cap.release()
cv2.destroyAllWindows()

"""
def simulate_typing(text):
    keyboard.write(text)
    time.sleep(1)  # Adjust as needed
"""
"""
# Example usage
simulate_typing("Hello, this is simulated typing.")


# Get the current mouse position
print(f"Current mouse position: {mouse.get_position()}")

# Move the mouse to a specific position
mouse.move(100, 100, absolute=True, duration=0.5)

# Click the left mouse button
mouse.click()

# Right-click at the current position
mouse.right_click()

# Scroll the mouse wheel up
mouse.wheel(1)

# Scroll the mouse wheel down
mouse.wheel(-1)

# Run the program for a few seconds
time.sleep(5)
"""