import cv2
import os

def process_image(input_path, output_path):
    # Read the image
    img = cv2.imread(input_path)

    # Convert the image to grayscale
    grayscale_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply blur with a 3x3 kernel
    blurred_img = cv2.GaussianBlur(grayscale_img, (3, 3), 0)

    # Apply dilation with a 2x2 kernel
    dilated_img = cv2.dilate(blurred_img, np.ones((2, 2), np.uint8), iterations=1)

    # Apply erosion with a 2x2 kernel
    eroded_img = cv2.erode(dilated_img, np.ones((2, 2), np.uint8), iterations=1)

    # Save the processed image
    output_filename = os.path.join(output_path, os.path.basename(input_path))
    cv2.imwrite(output_filename, eroded_img)

# Specify input and output folders
input_folder = '/path/to/input/folder'
output_folder = '/path/to/output/folder'

# Create the output folder if it doesn't exist
os.makedirs(output_folder, exist_ok=True)

# Process each PNG file in the input folder
#for filename in os.listdir(input_folder):
#    if filename.lower().endswith('.png'):
#        input_path = os.path.join(input_folder, filename)
#        process_image(input_path, output_folder)

#print("Processing complete.")


import cv2
import numpy as np

def nothing(x):
    pass

# Load an image
image = cv2.imread(R'C:\Users\Mikkel\source\repos\PythonApp_0001\processed dataset RGB v2\foward\image_2024-01-16_09.47.33_423.png')
cv2.namedWindow('HSV Color Trackbars')

# Convert the image to HSV color space
hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

# Create trackbars for color range adjustment
cv2.createTrackbar('Hue Min', 'HSV Color Trackbars', 0, 179, nothing)
cv2.createTrackbar('Hue Max', 'HSV Color Trackbars', 179, 179, nothing)
cv2.createTrackbar('Saturation Min', 'HSV Color Trackbars', 0, 255, nothing)
cv2.createTrackbar('Saturation Max', 'HSV Color Trackbars', 255, 255, nothing)
cv2.createTrackbar('Value Min', 'HSV Color Trackbars', 0, 255, nothing)
cv2.createTrackbar('Value Max', 'HSV Color Trackbars', 255, 255, nothing)

while True:
    # Exit loop if 'q' key is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # Get current trackbar positions
    hue_min = cv2.getTrackbarPos('Hue Min', 'HSV Color Trackbars')
    hue_max = cv2.getTrackbarPos('Hue Max', 'HSV Color Trackbars')
    sat_min = cv2.getTrackbarPos('Saturation Min', 'HSV Color Trackbars')
    sat_max = cv2.getTrackbarPos('Saturation Max', 'HSV Color Trackbars')
    val_min = cv2.getTrackbarPos('Value Min', 'HSV Color Trackbars')
    val_max = cv2.getTrackbarPos('Value Max', 'HSV Color Trackbars')

    # Create lower and upper bounds for the HSV color range
    lower_bound = np.array([hue_min, sat_min, val_min])
    upper_bound = np.array([hue_max, sat_max, val_max])

    # Create a mask using the HSV color range
    mask = cv2.inRange(hsv_image, lower_bound, upper_bound)

    # Apply the mask to the original image
    result = cv2.bitwise_and(image, image, mask=mask)

    # Display the original image and the result
    cv2.imshow('Original Image', image)
    cv2.imshow('Result', result)

cv2.destroyAllWindows()