import cv2
import os
import pandas as pd
import numpy as np

def process_image(input_path, output_path, blur_kernel=3, erosion_kernel=2, dilation_kernel=2):
    # Read the image
    img = cv2.imread(input_path)

    # Crop the sides of the image
    image = img[:, 275:-275, :]

    # Resize the image to target size
    image = cv2.resize(image, (244, 244))

    # Convert the image to grayscale
    #grayscale_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Apply blur with a specified kernel
    blurred_img = cv2.GaussianBlur(image, (blur_kernel, blur_kernel), 0)

    # Apply dilation with a specified kernel
    dilated_img = cv2.dilate(blurred_img, np.ones((dilation_kernel, dilation_kernel), np.uint8), iterations=1)

    # Apply erosion with a specified kernel
    eroded_img = cv2.erode(dilated_img, np.ones((erosion_kernel, erosion_kernel), np.uint8), iterations=1)

    # Save the processed image
    output_filename = os.path.join(output_path, os.path.basename(input_path))
    cv2.imwrite(output_filename, eroded_img)

    # Get class label from the input path
    class_label = os.path.basename(os.path.dirname(input_path))

    return {
        'Image Path': output_filename,
        'Class Label': class_label,
    }

def process_dataset(input_folder, output_folder):
    # Create the output folder if it doesn't exist
    os.makedirs(output_folder, exist_ok=True)

    # Create an empty list for annotations
    annotations_list = []

    # Process each class in the dataset
    for class_folder in os.listdir(input_folder):
        class_path = os.path.join(input_folder, class_folder)

        # Skip if it's not a directory
        if not os.path.isdir(class_path):
            continue

        # Create class-specific output folder
        class_output_folder = os.path.join(output_folder, class_folder)
        os.makedirs(class_output_folder, exist_ok=True)

        # Process each image in the class
        for file in os.listdir(class_path):
            if file.lower().endswith('.jpg'):
                input_path = os.path.join(class_path, file)

                # Process the image and get annotation
                annotation = process_image(input_path, class_output_folder)

                # Append the annotation to the list
                annotations_list.append(annotation)

    # Convert the list to a DataFrame
    annotations_df = pd.DataFrame(annotations_list)

    # Save the annotation CSV file outside all the classes
    annotation_csv_path = os.path.join(output_folder, 'annotations.csv')
    annotations_df.to_csv(annotation_csv_path, index=False)

    print("Processing complete.")

# Specify input and output folders
input_folder = R'C:\Users\Mikkel\source\repos\PythonApp_0001\dataset v3'
output_folder = R'C:\Users\Mikkel\source\repos\PythonApp_0001\processed dataset RGB v2'

# Process the entire dataset
process_dataset(input_folder, output_folder)