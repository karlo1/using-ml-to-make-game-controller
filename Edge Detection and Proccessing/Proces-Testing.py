import cv2
import numpy as np

# Function to update the contrast enhancement
def update_contrast(alpha):
    global image
    enhanced_image = cv2.convertScaleAbs(gray_image, alpha=alpha/10, beta=0)
    cv2.imshow('Processed Image', enhanced_image)

# Function to update the gamma correction
def update_gamma(gamma):
    global gray_image
    gamma_corrected = np.uint8(cv2.pow(gray_image / 255.0, gamma/10) * 255)
    cv2.imshow('Processed Image', gamma_corrected)

## Function to update the color normalization
#def update_normalization(norm_type):
#    global image
#    normalized_image = cv2.normalize(gray_image, None, alpha=0, beta=255, norm_type=norm_type)
#    cv2.imshow('Processed Image', normalized_image)

# Load the image
gray_image = cv2.imread(R"C:\Users\Mikkel\source\repos\PythonApp_0001\processed dataset RGB v2\foward\image_2024-01-16_09.47.33_423.jpg")

# Define the coordinates of the square region (top-left and bottom-right corners)
x1, y1 = 95, 70  # Top-left corner
x2, y2 = 150, 200  # Bottom-right corner

# Crop the square region from the image
square_region = gray_image[y1:y2, x1:x2]

# Calculate the average mean of pixel values in the square region
average_mean = np.mean(square_region)

print("Average mean of pixel values in the square region:", average_mean)

# Draw a rectangle on the original image to outline the square region
output_image = gray_image.copy()
cv2.rectangle(output_image, (x1, y1), (x2, y2), (0, 255, 0), 2)  # Draw a green rectangle

# Display the image with the rectangle
cv2.imshow('Square Region', output_image)





# Adjust initial values for contrast and gamma based on brightness
if average_mean < 150:  # Bright image
    initial_contrast = average_mean / 7
    initial_gamma = average_mean / 10
else:  # Dark image
    initial_contrast = average_mean / 10
    initial_gamma = average_mean / 8
# Create a window
cv2.namedWindow('Processed Image')

# Create trackbars for contrast enhancement, gamma correction, and color normalization
cv2.createTrackbar('Contrast', 'Processed Image', int(initial_contrast), 30, update_contrast)
cv2.createTrackbar('Gamma', 'Processed Image', int(initial_gamma), 30, update_gamma)
#cv2.createTrackbar('Normalization', 'Processed Image', 0, 3, update_normalization)

# Initialize with default parameters
update_contrast(int(initial_contrast))
update_gamma(int(initial_gamma))
#update_normalization(0)

# Wait for key press and close
cv2.waitKey(0)
cv2.destroyAllWindows()