import torch
from torch.utils.data import DataLoader
from torchvision import transforms
from PIL import Image
import os
from torchvision.datasets import ImageFolder
# Assuming your dataset is named 'custom_dataset'
# Replace 'custom_dataset' with the actual name of your dataset class

# Assuming your dataset is organized in folders, one for each class
data_root = r'C:\Users\Mikkel\source\repos\PythonApp_0001\processed dataset RGB v2'
classes = os.listdir(data_root)


# Transformation to apply to each image
transform = transforms.Compose([
   
    transforms.ToTensor(),
])

# Create an instance of ImageFolder dataset
# ImageFolder assumes your dataset is organized in folders, one for each class
dataset = ImageFolder(root=data_root, transform=transform)

# Set batch size to a reasonable value
batch_size = 12

# Create a DataLoader to iterate over the dataset in batches
data_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True)

# Calculate mean and standard deviation over the dataset
mean = torch.zeros(3)  # RGB image has three channels
std = torch.zeros(3)

total_samples = 0

for data in data_loader:
    inputs, _ = data
    batch_samples = inputs.size(0)
    channels = inputs.size(1)

    # Calculate mean and std along each channel
    mean += inputs.view(batch_samples, channels, -1).mean(2).sum(0)
    std += inputs.view(batch_samples, channels, -1).std(2).sum(0)

    total_samples += batch_samples

# Normalize by the total number of samples
mean /= total_samples
std /= total_samples

print(f'Mean: {mean}')
print(f'Standard deviation: {std}')