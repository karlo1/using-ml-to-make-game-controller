import cv2
import os

def preprocess_image(input_path, output_path, crop_margin=275, target_size=(244, 244)):
    # Iterate through all files in the input directory
    for file_name in os.listdir(input_path):
        # Construct the full path for input and output
        input_file_path = os.path.join(input_path, file_name)
        output_file_path = os.path.join(output_path, file_name)

        # Open the image using OpenCV
        image = cv2.imread(input_file_path)

        # Crop the sides of the image
        image = image[:-10, 20:-5,:]

        # Resize the image to target size
        image = cv2.resize(image, target_size)

        # Save the preprocessed image
        cv2.imwrite(output_file_path, image)

# Example usage
input_path = r'C:\Users\Mikkel\source\repos\PythonApp_0001\processed dataset RGB v2\kick'
output_path = r'C:\Users\Mikkel\Desktop\output'
preprocess_image(input_path, output_path)
