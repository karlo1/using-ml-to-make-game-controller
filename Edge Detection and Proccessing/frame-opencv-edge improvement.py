import cv2
import numpy as np

# Callback function for trackbar changes
def update_params(x):
    pass  # Placeholder function, as we'll use the trackbar callback

# Create a window to display the webcam feed
cv2.namedWindow('Image Processing')

# Create trackbars for parameter adjustments
cv2.createTrackbar('Blur', 'Image Processing', 3, 100, update_params)
cv2.createTrackbar('Erosion', 'Image Processing', 2, 100, update_params)
cv2.createTrackbar('Dilation', 'Image Processing', 2, 100, update_params)

# Open a connection to the camera (use 0 for the default camera)
cap = cv2.VideoCapture(0)

while True:
    # Read a frame from the camera
    ret, frame = cap.read()

    # Resize the frame to 244x244
    frame_resized = cv2.resize(frame, (244, 244))

    # Get current trackbar positions
    blur_kernel = cv2.getTrackbarPos('Blur', 'Image Processing')
    erosion_iterations = cv2.getTrackbarPos('Erosion', 'Image Processing')
    dilation_iterations = cv2.getTrackbarPos('Dilation', 'Image Processing')

    # Apply blur to the resized frame
    blurred_frame = cv2.GaussianBlur(frame_resized, (2 * blur_kernel + 1, 2 * blur_kernel + 1), 0)

    # Apply erosion to the blurred frame
    eroded_frame = cv2.erode(blurred_frame, np.ones((3, 3), np.uint8), iterations=erosion_iterations)

    # Apply dilation to the eroded frame
    dilated_frame = cv2.dilate(eroded_frame, np.ones((3, 3), np.uint8), iterations=dilation_iterations)

    # Convert the frames to HSV and grayscale
    hsv_frame = cv2.cvtColor(dilated_frame, cv2.COLOR_BGR2HSV)
    grayscale_frame = cv2.cvtColor(dilated_frame, cv2.COLOR_BGR2GRAY)

    # Display the frames side by side
    comparison_frame = np.hstack((frame_resized, cv2.cvtColor(hsv_frame, cv2.COLOR_HSV2BGR), cv2.cvtColor(grayscale_frame, cv2.COLOR_GRAY2BGR)))
    cv2.imshow('Image Processing', comparison_frame)

    # Break the loop if 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the camera and close the OpenCV window
cap.release()
cv2.destroyAllWindows()
